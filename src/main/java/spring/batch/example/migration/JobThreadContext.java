package spring.batch.example.migration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadFactory;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class JobThreadContext {
  private static final Logger LOGGER = LoggerFactory.getLogger(JobThreadContext.class);

  private Thread jobThread;

  @Autowired
  private ThreadFactory tf;

  public void interruptCurrentThread() {
    if (nonNull(jobThread) && jobThread.isAlive()) {
      jobThread.interrupt();
      LOGGER.info("Interrupting current job thread id: [{}]", jobThread.getId());
    }

    jobThread = null;
  }

  public Thread.State startNewThread(Runnable runnable) throws InterruptedException {
    if (isNull(jobThread)) {
      jobThread = tf.newThread(runnable);
      jobThread.start();

      return Thread.State.NEW;
    }

    return Thread.State.RUNNABLE;
  }

  public Thread getJobThread() {
    return jobThread;
  }
}
