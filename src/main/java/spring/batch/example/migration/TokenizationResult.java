package spring.batch.example.migration;

public class TokenizationResult {
  private long value;
  private boolean error;
  private String errorDescription;

  public long getValue() {
    return value;
  }

  public void setValue(long value) {
    this.value = value;
  }

  public boolean isError() {
    return error;
  }

  public void setError(boolean error) {
    this.error = error;
  }

  public String getErrorDescription() {
    return errorDescription;
  }

  public void setErrorDescription(String errorDescription) {
    this.errorDescription = errorDescription;
  }
}
