package spring.batch.example.migration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Configuration
@ConfigurationProperties(prefix = "circuit-break")
@Scope(value = SCOPE_PROTOTYPE)
public class CircuitBreakerProperties {
  private Integer errorThreshold;
  private Integer waitTimeInMilliseconds;
  private Integer stepFactor;
  private Integer retryCount;

  public void updateCircuitBreakerProperties(int sleepTime) {
    this.setWaitTimeInMilliseconds(sleepTime);
    this.setRetryCount(this.getRetryCount()-1);
    this.setWaitTimeInMilliseconds(sleepTime * this.getStepFactor());
  }

  public Integer getErrorThreshold() {
    return errorThreshold;
  }

  public void setErrorThreshold(Integer errorThreshold) {
    this.errorThreshold = errorThreshold;
  }

  public Integer getWaitTimeInMilliseconds() {
    return waitTimeInMilliseconds;
  }

  public void setWaitTimeInMilliseconds(Integer waitTimeInMilliseconds) {
    this.waitTimeInMilliseconds = waitTimeInMilliseconds;
  }

  public Integer getStepFactor() {
    return stepFactor;
  }

  public void setStepFactor(Integer stepFactor) {
    this.stepFactor = stepFactor;
  }

  public Integer getRetryCount() {
    return retryCount;
  }

  public void setRetryCount(Integer retryCount) {
    this.retryCount = retryCount;
  }
}
