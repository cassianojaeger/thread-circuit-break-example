package spring.batch.example.migration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Service
@Scope(value = SCOPE_PROTOTYPE)
public class CircuitBreaker {
  private static final Logger LOGGER = LoggerFactory.getLogger(CircuitBreaker.class);

  @Autowired
  JobThreadContext jobThreadContext;
  @Autowired
  ApplicationContext applicationContext;
  private boolean hadPreviousError;

  private CircuitBreakerProperties circuitBreakerProperties;

  public CircuitBreaker(CircuitBreakerProperties circuitBreakerProperties) {
    this.circuitBreakerProperties = circuitBreakerProperties;
  }

  public void monitorTokenizationJob(List<TokenizationResult> tokenizationResults) throws InterruptedException {
    long batchSize = tokenizationResults.size();
    long quantityOfErrors = tokenizationResults.stream().filter(TokenizationResult::isError).count();

    long errorPercentage = 100 * quantityOfErrors / batchSize;
    LOGGER.info("Error percentage: {}%", errorPercentage);

    if(noMoreRetriesLeft()) {
      LOGGER.info("Circuit Break retry count exceeded. Terminating tokenization job thread");
      jobThreadContext.interruptCurrentThread();
    }
    else if(errorSurpassedThreshold(errorPercentage)) {
      triggerExecutionDelay();
    }
    else if (hadPreviousError) {
      circuitBreakerProperties = (CircuitBreakerProperties) applicationContext.getBean("circuitBreakerProperties");
      hadPreviousError = false;
    }
  }

  private boolean errorSurpassedThreshold(long errorPercentage) {
    return errorPercentage > circuitBreakerProperties.getErrorThreshold();
  }

  private boolean noMoreRetriesLeft() {
    return circuitBreakerProperties.getRetryCount() <= 0;
  }

  private void triggerExecutionDelay() throws InterruptedException {
    int sleepTime = circuitBreakerProperties.getWaitTimeInMilliseconds();
    circuitBreakerProperties.updateCircuitBreakerProperties(sleepTime);

    LOGGER.info("Sleep delay: {}ms - Retry Count left: {} - Next Sleep delay: {}ms",
        sleepTime,
        circuitBreakerProperties.getRetryCount(),
        circuitBreakerProperties.getWaitTimeInMilliseconds()
    );

    hadPreviousError = true;
    Thread.sleep(sleepTime);
  }
}
