package spring.batch.example.migration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Service
public class TokenizationService {

  private static final Logger LOGGER = LoggerFactory.getLogger(TokenizationService.class);
  public static final String JOB_STATE_MESSAGE = "TOKENIZATION JOB IS IN THE CURRENT STATE: %s";

  @Autowired
  JobThreadContext jobThreadContext;
  @Autowired
  CircuitBreaker circuitBreaker;

  public void interruptTokenizationJob() {
      jobThreadContext.interruptCurrentThread();
  }

  public String createTokenizationThread() throws InterruptedException {
    Runnable runnable = this::startTokenizationProcess;
    Thread.State state = jobThreadContext.startNewThread(runnable);

    return String.format(JOB_STATE_MESSAGE, state.name());
  }

  private synchronized void startTokenizationProcess() {
    long firstNum = 1;
    long lastNum = 50;

    List<Long> aList = LongStream.rangeClosed(firstNum, lastNum).boxed()
        .collect(Collectors.toList());

    try {
      do {
        //TODO: STRATEGY TO WAIT 1S PER CHUNK OF DATA PROCESSED - calculate time
        Instant start = Instant.now();
        List<TokenizationResult> results = aList.parallelStream().map(this::tokenizeExample).collect(Collectors.toList());
        Instant finish = Instant.now();
        //TODO: STRATEGY TO WAIT 1S PER CHUNK OF DATA PROCESSED - calculate time
        long timeElapsed = Duration.between(start, finish).toMillis();
        LOGGER.info("Time elapsed {}", timeElapsed);

        //TODO: STRATEGY TO WAIT 1S PER CHUNK OF DATA PROCESSED - verify if needs to wait
        if (timeElapsed < 1000) {
          long sleepTime = 1000 - timeElapsed; //TODO: STRATEGY TO WAIT 1S PER CHUNK OF DATA PROCESSED - wait only required left amount
          LOGGER.info("Waiting {} milliseconds", sleepTime);
          Thread.sleep(sleepTime);
        }

        circuitBreaker.monitorTokenizationJob(results);

      } while (true);
    } catch (Exception e) {
      LOGGER.info("Catching exception: [{}]", e.getMessage());
    } finally {
      LOGGER.info("Cleaning thread context - removing enclosing thread");
      jobThreadContext.interruptCurrentThread();
    }
  }

  private TokenizationResult tokenizeExample(Long number) {
    TokenizationResult result = new TokenizationResult();
    result.setValue(number*2);
    result.setError(true);

    return result;
  }
}
