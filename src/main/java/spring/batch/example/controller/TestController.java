package spring.batch.example.controller;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import spring.batch.example.migration.TokenizationService;

@RestController
public class TestController {

  @Autowired
  TokenizationService tokenizationService;

  @GetMapping("/startProcess")
  public String handleStart() throws Exception{
    return tokenizationService.createTokenizationThread();
  }

  @GetMapping("/cancelProcess")
  public void handleCancel() throws Exception{
    tokenizationService.interruptTokenizationJob();
  }
}
